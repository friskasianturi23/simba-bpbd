<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subdistrict extends Model
{
    protected $fillable = ['name'];


    public function villages()
    {
        return $this->hasMany(Village::class);
    }
}
