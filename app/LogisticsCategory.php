<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogisticsCategory extends Model
{
    protected $fillable = ['name'];

    public function logistics()
    {
        return $this->hasMany(Logistics::class);
    }
}
