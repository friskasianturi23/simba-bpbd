<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logistics extends Model
{

  protected $primaryKey = 'id';

  protected $fillable = [
    'nama_barang',
    'logisticscategory_id',
    'jumlah_ketersediaan',
    'instansi',
    'kondisi',
    'tanggal_diterimanya_barang',
    'nomor_surat_tanggal_penyerahan',
    'tanggal_kadaluarsa_barang',
    'tahun_perolehan'
  ];

  public function logisticscategory()
  {
    return $this->belongsTo(LogisticsCategory::class);
  }
}
