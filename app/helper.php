<?php

use Carbon\Carbon;

function formatDate($date)
{
    return Carbon::parse($date)->translatedFormat('d F Y');
}
