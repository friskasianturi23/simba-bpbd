<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Report extends Model
{
    protected $casts = [
        'validated' => 'boolean'
    ];
    protected $fillable = [
        'disaster_id',
        'village_id',
        'incident_time',
        'description',
        'ownership',
        'street',
        'image',
        'name',
        'phone_number',
        'nik'
    ];
    public function deleteImage()
    {
        Storage::delete($this->image);
    }

    public function disaster()
    {
        return $this->belongsTo(Disaster::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
    public function village()
    {
        return $this->belongsTo(Village::class);
    }
}
