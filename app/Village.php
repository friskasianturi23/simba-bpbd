<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $fillable = ['subdistrict_id', 'name'];

    public function locations()
    {
        return $this->hasMany(Location::class);
    }
    public function subdistrict()
    {
        return $this->belongsTo(Subdistrict::class);
    }
    public function reports()
    {
        $this->hasMany(Report::class);
    }
}
