<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disaster extends Model
{
    protected $fillable = ['name', 'description'];
    public function reports()
    {
        return $this->hasMany(Report::class);
    }
}
