<?php

namespace App\Http\Controllers;

use App\Subdistrict;
use App\Village;
use Illuminate\Http\Request;

class VillageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Subdistrict $subdistrict)
    {
        return view('admin.reports.villages.index', compact('subdistrict'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Subdistrict $subdistrict)
    {
        return view('admin.reports.villages.create', compact('subdistrict'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Subdistrict $subdistrict)
    {
        $subdistrict->villages()->create($this->validateRequest());

        return redirect(route('villages.index', compact('subdistrict')));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Village  $village
     * @return \Illuminate\Http\Response
     */
    public function show(Village $village)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Village  $village
     * @return \Illuminate\Http\Response
     */
    public function edit(Subdistrict $subdistrict, Village $village)
    {

        return view('admin.reports.villages.edit', compact([
            'subdistrict',
            'village'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Village  $village
     * @return \Illuminate\Http\Response
     */
    public function update(Subdistrict $subdistrict, Village $village)
    {
        $village->update($this->validateRequest());
        return redirect(route('villages.index', compact('subdistrict')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Village  $village
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subdistrict $subdistrict, Village $village)
    {
        $village->delete();
        return redirect(route('villages.index', compact('subdistrict')));
    }
    public function validateRequest()
    {
        return request()->validate([
            'name' => 'required'
        ]);
    }
    public function findVillagesById(Request $request)
    {
        //echo json_encode(DB::table('sub_categories')->where('category_id', $id))->get();
        $data = Village::select('name', 'id')->where(
            'subdistrict_id',
            $request->subs_id
        )->get();
        return  response()->json($data);
    }
}
