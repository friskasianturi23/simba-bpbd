<?php

namespace App\Http\Controllers;

use App\Subdistrict;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubdistrictController extends Controller
{
    public function index()
    {
        $subdistricts  = Subdistrict::all();

        return view('admin.reports.subdistricts.index', compact('subdistricts'));
    }

    public function create()
    {
        return view('admin.reports.subdistricts.create');
    }

    public function store()
    {
        Subdistrict::create(request()->validate([
            'name' => 'required|unique:subdistricts'
        ]));

        return redirect(route('subdistricts.index'));
    }

    public function edit(Subdistrict $subdistrict)
    {
        return view('admin.reports.subdistricts.edit', compact('subdistrict'));
    }

    public function update(Subdistrict $subdistrict)
    {
        $subdistrict->update(request()->validate([
            'name' => 'required'
        ]));

        return redirect(route('subdistricts.index'));
    }

    public function destroy(Subdistrict $subdistrict)
    {
        if ($subdistrict->villages->count()) {
            session()->flash('error', 'Gagal Menghapus karena Kecamatan ini memiliki desa');
        } else {
            $subdistrict->delete();
            session()->flash('success', 'Berhasil menghapus desa');
        }
        return redirect(route('subdistricts.index'));
    }
    public function findVillagesById()
    {

        $subdistrict = Subdistrict::find(request(['subs_id']));

        //  return response()->json($subdistrict->villages->pluck(['id', 'name']));
        return response()->json($subdistrict);
        // return response()->json($subdistrict);
    }
}
