<?php

namespace App\Http\Controllers;

use App\Http\Requests\logistics\CreateLogisticsRequest;
use App\Logistics;
use App\JenisPaketLogistiks;
use App\LogisticsCategory;
use Illuminate\Http\Request;

class LogisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logistiks = Logistics::paginate(7); //->where('nama_barang', 'like', "%".$cari."%");
        return view('admin.logistics.index')->with('logistiks', $logistiks);
    }

    public function search(Request $request)
    {
        $search = $request->get('q');
        $logistics = Logistics::where('nama_barang', 'LIKE', '%' . $search . '%')
            //  ->orWhere('jenis_paket_logistik', 'LIKE', '%' . $search . '%')
            ->orWhere('kondisi', 'LIKE', '%' . $search . '%')
            ->orWhere('tahun_perolehan', 'LIKE', '%' . $search . '%')
            ->paginate(7);

        return view('admin.logistics.result', compact('search', 'logistics'));
    }

    public function create()
    {
        $jenispaketlogistiks = LogisticsCategory::pluck('name', 'id');

        return view('admin.logistics.create', compact('jenispaketlogistiks'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateLogisticsRequest $request)
    {

        Logistics::create($this->validateRequest($request));
        session()->flash('success', 'Berhasil membuat data logistik yang baru');

        return redirect(route('logistics.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Logistiks  $logistiks
     * @return \Illuminate\Http\Response
     */
    public function show(Logistics $logistic)
    {
        // dd($logistic);
        // $logistiks = Logistics::findOrFail($id);
        return view('admin.logistics.show', compact('logistic'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Logistiks  $logistiks
     * @return \Illuminate\Http\Response
     */
    public function edit(Logistics $logistic)
    {
        $jenispaketlogistiks = LogisticsCategory::all();

        return view('admin.logistics.edit', compact(['logistic', 'jenispaketlogistiks']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Logistiks  $logistiks
     * @return \Illuminate\Http\Response
     */
    public function update(CreateLogisticsRequest $request, Logistics $logistic)
    {
        //  dd($request);
        $logistic->update($this->validateRequest($request));

        return redirect()->route('logistics.index')->with('success', 'Data telah diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Logistics  $logistiks
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $logistiks = Logistics::find($id);
        $logistiks->delete();

        return redirect()->route('logistics.index')->with('success', 'Data telah dihapus!');
    }

    public function validateRequest($request)
    {
        return [
            'nama_barang' => $request['nama_barang'],
            'logisticscategory_id' => $request['logisticscategory_id'],
            'jumlah_ketersediaan' => $request['jumlah_ketersediaan'],
            'instansi' => $request['instansi'],
            'kondisi' => $request['kondisi'],
            'tanggal_diterimanya_barang' => $request['tanggal_diterimanya_barang'],
            'nomor_surat_tanggal_penyerahan' => $request['nomor_surat_tanggal_penyerahan'],
            'tanggal_kadaluarsa_barang' => $request['tanggal_kadaluarsa_barang'],
            'tahun_perolehan' => $request['tahun_perolehan']
        ];
    }
}
