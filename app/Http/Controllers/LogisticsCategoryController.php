<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\logistics_categories\CreateLogisticsCategoryRequest;
use App\Http\Requests\logistics_categories\UpdateLogisticsCategoryRequest;
use App\LogisticsCategory;
use Illuminate\Http\Request;

class LogisticsCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logisticsCategories = LogisticsCategory::all();
        return view('admin.logistics_categories.index', compact('logisticsCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.logistics_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateLogisticsCategoryRequest $request)
    {
        LogisticsCategory::create([
            'name' => $request['name'],
        ]);
        session()->flash('success', 'Berhasil membuat kategory barang logistik');

        return redirect(route('logistics-categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LogisticsCategory  $logisticsCategory
     * @return \Illuminate\Http\Response
     */
    public function show(LogisticsCategory $logisticsCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LogisticsCategory  $logisticsCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(LogisticsCategory $logisticsCategory)
    {
        return view('admin.logistics_categories.edit', compact('logisticsCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LogisticsCategory  $logisticsCategory
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLogisticsCategoryRequest $request, LogisticsCategory $logisticsCategory)
    {
        $logisticsCategory->update([
            'name' => $request['name'],
        ]);

        session()->flash('success', 'Berhasil mengedit kategory barang logistik');

        return redirect(route('logistics-categories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LogisticsCategory  $logisticsCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(LogisticsCategory $logisticsCategory)
    {
        if ($logisticsCategory->logistics->count()) {
            session()->flash('error', 'Tidak dapat menghapus kategori logistik karena sudah digunakan dalam data logistik');
            return redirect(route('logistics-categories.index'));
        }
        $logisticsCategory->delete();
        session()->flash('success', 'Berhasil menghapus kategory');
        return redirect(route('logistics-categories.index'));
    }
}
