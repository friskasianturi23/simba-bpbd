<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Category;
use App\Report;
use App\Subdistrict;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $announcements =  DB::table('announcements')->join('categories', function ($join) {
            $join->on('announcements.category_id', '=', 'categories.id')->where('categories.name', '<>', 'Berita');
        })->get();

        $activities = DB::table('announcements')->join('categories', function ($join) {
            $join->on('announcements.category_id', '=', 'categories.id')->where('categories.name', 'Berita');
        })->get();

        return view('home', compact(['announcements', 'activities']));
    }
    public function admin()
    {
        $reportsCount = Report::count();
        $newsCount = Announcement::count();
        $subdistrictCount = Subdistrict::count();

        return view('admin.index', compact(['newsCount', 'reportsCount', 'subdistrictCount']));
    }
}
