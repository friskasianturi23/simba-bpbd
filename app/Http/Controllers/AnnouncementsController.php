<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Category;
use App\Http\Requests\announcements\CreateAnnouncementsRequest;
use App\Http\Requests\announcements\UpdateAnnouncementsRequest;

use Illuminate\Http\Request;

class AnnouncementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announcements = Announcement::all();
        return view('admin.announcements.index', compact('announcements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.announcements.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAnnouncementsRequest $request)
    {
        // dd($request);
        $image = request()->image->store('announcements');

        Announcement::create([
            'title' => $request['title'],
            'body' => $request['body'],
            'announcement_date' => $request['announcement_date'],
            'image' => $image,
            'category_id' => $request['category'],
        ]);

        session()->flash('success', 'Berhasil membuat berita baru');

        return redirect(route('announcements.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        return view('admin.announcements.show', compact('announcement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        $categories = Category::all();
        return view('admin.announcements.edit', compact([
            'announcement',
            'categories'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAnnouncementsRequest $request, Announcement $announcement)
    {
        $data = $request->only(['title', 'body', 'category_id']);

        if ($request->hasFile('image')) {
            $image = $request->image->store('announcements');
            $announcement->deleteImage();
            $data['image'] = $image;
        }
        $announcement->update($data);
        session()->flash('success', 'Berhasil memperbarui berita');
        return redirect(route('announcements.show', compact('announcement')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        $announcement->deleteImage();
        $announcement->delete();
        return redirect(route('announcements.index'));
    }
    public function getNews()
    {
        $announcements = Announcement::where('category_id', 1)->get();
        return view('guests.announcements.index', compact('announcements'));
    }
    public function getActivities()
    {
        $announcements = Announcement::where('category_id', '<>', 1)->get();
        return view('guests.announcements.index', compact('announcements'));
    }

    public function showActivity(Announcement $announcement)
    {
        return view('guests.announcements.show', compact('announcement'));
    }
    public function showNews(Announcement $announcement)
    {
        return view('guests.announcements.show', compact('announcement'));
    }
}
