<?php

namespace App\Http\Controllers;

use App\Announcement;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class GuestHomeController extends Controller
{
    public function index()
    {

        $announcements =  Announcement::where('category_id', 1)->paginate(10);
        // DB::table('announcements')->join('categories', function ($join) {
        //     $join->on('announcements.category_id', '=', 'categories.id')->where('categories.name', '<>', 'Berita');
        // })->get();

        $activities = Announcement::where('category_id', '<>', 1)->paginate(10);
        //  DB::table('announcements')->join('categories', function ($join) {
        //     $join->on('announcements.category_id', '=', 'categories.id')->where('categories.name', 'Berita');
        // })->get();

        return view('guests.index', compact(['announcements', 'activities']));
    }
    public function galery()
    {
        return view('guests.galeri');
    }
}
