<?php

namespace App\Http\Controllers;

use App\Disaster;
use App\Http\Requests\reports\CreateReportRequest;
use App\Http\Requests\reports\UpdateReportRequest;
use App\Report;
use App\Subdistrict;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::all();
        return view('admin.reports.index', compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $disasters = Disaster::all();
        $subdistricts = Subdistrict::all();
        return view('admin.reports.create', compact(['disasters', 'subdistricts']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateReportRequest $request)
    {
        $image = request()->image->store('reports');

        $report =  Report::create([
            'disaster_id' => $request['disaster'],
            'incident_time' => $request['incident_time'],
            'description' => $request['description'],
            'ownership' =>  $request['ownership'],
            'street' => $request['street'],
            'village_id' => $request['village'],
            'image' => $image,
            'name' => $request['name'],
            'phone_number' => $request['phone_number'],
            'nik' => $request['nik'],
        ]);
        session()->flash('success', "Tenang...! Laporan kamu telah diproses ke pihak BPBD Tebing Tinggi");
        if (Auth::user()) {
            return redirect(route('reports.show', compact('report')));
        }
        return redirect(route('reports.guest.show', compact('report')));
    }

    public function showReport(Report $report)
    {
        return view('guests.reports.show', compact('report'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        return view('admin.reports.show', compact('report'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateReportRequest $request, Report $report)
    {
        $data = $request->only([
            'disaster_id',
            'incident_time',
            'description',
            'ownership',
            'location',
            'name',
            'phone_number',
            'nik'
        ]);

        if ($request->hasFile('image')) {
            $image = $request->image->store('reports');
            $report->deleteImage();
            $data['image'] = $image;
        }
        $report->update($data);

        session()->flash('success', 'Berhasil memperbarui berita');
        return redirect('/admin');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        return view('admin.reports.edit', compact('report'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        $report->deleteImage();
        $report->delete();
        return redirect(route('reports.index'));
    }
    public function createReportByGuest()
    {
        $disasters = Disaster::all();
        $subdistricts = Subdistrict::all();
        return view('guests.reports.create', compact(['disasters', 'subdistricts']));
    }

    public function validateReport(Report $report)
    {
        $report->validated = true;
        $report->save();

        session()->flash('success', "Laporan telah berhasil divalidasi");

        return redirect(route('reports.index'));
    }
}
