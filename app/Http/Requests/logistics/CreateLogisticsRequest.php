<?php

namespace App\Http\Requests\logistics;

use Illuminate\Foundation\Http\FormRequest;

class CreateLogisticsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_barang' => 'required|string',
            'logisticscategory_id' => 'required',
            'jumlah_ketersediaan' => 'required|numeric|min:1',
            'instansi' => 'required|string',
            'kondisi' => 'required|string',
            'tanggal_diterimanya_barang' => 'required|date',
            'nomor_surat_tanggal_penyerahan' => 'required|string',
            'tahun_perolehan' => 'required|numeric|min:1',
        ];
    }
    public function messages()
    {
        return [
            'nama_barang.required' => 'Nama barang tidak boleh kosong',
            'logisticscategory_id.required' => 'Nama kategory barang logistik tidak boleh kosong',
            'jumlah_ketersediaan.required' => 'Jumlah ketersediaan barang tidak boleh kosong',
            'instansi.required' => 'Nama Instansi tidak boleh kosong',
            'kondisi.required' => 'Kondisi barang tidak boleh kosong',
            'tanggal_diterimanya_barang.required' => 'Tanggal diterimanya barang tidak boleh kosong',
            'nomor_surat_tanggal_penyerahan.required' => 'Nomor surat tidak boleh kosong',
            'tahun_perolehan.required' => 'Tahun perolehan tidak boleh kosong',
        ];
    }
}
