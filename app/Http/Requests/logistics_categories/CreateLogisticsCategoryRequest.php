<?php

namespace App\Http\Requests\logistics_categories;

use Illuminate\Foundation\Http\FormRequest;

class CreateLogisticsCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3, unique:logistics_categories'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Nama jenis barang logistik tidak boleh kosong',
            'name.unique' => 'Nama sudah pernah digunakan',
        ];
    }
}
