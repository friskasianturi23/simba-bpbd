<?php

namespace App\Http\Requests\reports;

use Illuminate\Foundation\Http\FormRequest;

class UpdateReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'disaster_id' => 'required',
            'incident_time' => 'required',
            'description' => 'required',
            'ownership' => 'required',
            'location' => 'required',
            'name' => 'required',
            'phone_number' => 'required'
        ];
    }
}
