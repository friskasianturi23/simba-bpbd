<?php

namespace App\Http\Requests\reports;

use Illuminate\Foundation\Http\FormRequest;

class CreateReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'disaster' => 'required',
            'incident_time' => 'required',
            'village' => 'required',
            'description' => 'required',
            'ownership' => 'required',
            'street' => 'required',
            'image' => 'required|image',
            'name' => 'required',
            'phone_number' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'disaster.required' => 'Jenis bencana tidak boleh kosong',
            'incident_time.required' => 'Waktu kejadian bencana tidak boleh kosong',
            'village.required' => 'Nama Desa/Keluranan tidak boleh kosing',
            'description.required' => 'Deskripsi Kejadian tidak boleh kosong',
            'ownership.required' => 'Kepemilikan tempat tidak boleh kosong',
            'street.required' => 'Jalan kejadian/bencana tidak boleh kosong',
            'image.required' => 'Gambar tidak boleh kosong',
            'name.required' => 'Nama pelapor tidak boleh kosong',
            'phone_number.required' => 'Nomor telepon pelapor tidak boleh kosong'
        ];
    }
}
