<?php

namespace App\Http\Requests\announcements;

use Illuminate\Foundation\Http\FormRequest;

class CreateAnnouncementsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'body'  => 'required',
            'image' => 'required|image',
            'announcement_date' => 'required',
            'category' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Judul tidak boleh kosong',
            'body.required' => 'Isi berita tidak kosong',
            'image.required' => 'Gambar tidak boleh kosong',
            'image.image' => 'File yang anda masukan bukan berupa gambar',
            'announcement_data.required' => 'Tanggal kejadian/berita tidak boleh kosong',
            'category.required' => 'Kategori berita tidak boleh kosong',
        ];
    }
}
