<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPaketLogistiks extends Model
{

  protected $table = 'jenispaketlogistiks';
  protected $primaryKey = 'id';

  public function logistiks(){
    return $this->hasMany('Logistiks::class');
  }
}
