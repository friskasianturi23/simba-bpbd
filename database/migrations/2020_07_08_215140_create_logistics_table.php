<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('logistics', function (Blueprint $table) {
            $table->id();
            $table->string('nama_barang');
            //    $table->string('jenis_paket_logistik');
            $table->integer('jumlah_ketersediaan');
            $table->string('instansi');
            $table->string('kondisi');
            $table->date('tanggal_diterimanya_barang')->nullable();
            $table->string('nomor_surat_tanggal_penyerahan');
            $table->date('tanggal_kadaluarsa_barang')->nullable();
            $table->integer('tahun_perolehan');
            $table->unsignedBigInteger('logistics_category_id');
            $table->timestamps();

            $table->foreign('logistics_category_id')
                ->references('id')->on('logistics_categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logistiks');
        Schema::drop('jenis_paket_logistiks');
    }
}
