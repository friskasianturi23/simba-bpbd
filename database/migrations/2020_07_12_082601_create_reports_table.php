<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->id()->index();
            $table->unsignedBigInteger('disaster_id');
            $table->date('incident_time');
            $table->text('description');
            $table->string('ownership');
            $table->string('street');
            $table->unsignedBigInteger('village_id');
            $table->string('image');
            $table->string('name');
            $table->string('phone_number');
            $table->string('nik')->nullable();
            $table->timestamps();

            $table->foreign('disaster_id')
                ->references('id')
                ->on('disasters')
                ->onDelete('cascade');

            $table->foreign('village_id')
                ->references('id')
                ->on('villages')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
