@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-8">
        <div class="row">
            <span><a href="{{route('categories.index')}}" class="mx-4">Jenis Berita</a></span>
            <span><a href="{{route('disasters.index')}}" class="mx-4">Jenis Bencana</a></span>
            <span><a href="{{route('announcements.index')}}" class="mx-4">Berita</a></span>
            <span><a href="{{route('logistiks.index')}}" class="mx-4">Logistik</a></span>
        </div>
    </div>
</div>
@endsection
