@extends('admin.app')
@section('content')
<div class="container">
    <div class="col-md-8">
        <div class="card card-default">
            <div class="card-header">Buat Berita</div>
            <form action="{{route('announcements.store')}}" method="POST" enctype="multipart/form-data">
                @include('admin.announcements.form',[
                'buttonText' => 'Create',
                'announcement'=>null
                ])
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.css">
@endsection