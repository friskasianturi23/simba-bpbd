@csrf
<div class="card-body">
    <div class="form-group">
        <label for="title">Title</label>
        <input type="title" name="title" id="title" class="form-control" value="{{isset($announcement) ? $announcement->title :''}}">
        @if($errors->has('title'))
        <p class="text-danger text-sm">{{$errors->first('title')}}</p>
        @endif
    </div>
    <div class="form-group">
        <label for="announcement_date">Waktu kejadian </label>
        <input type="text" name="announcement_date" id="announcement_date" class="form-control" value="{{isset($announcement)?$announcement->announcement_name:''}}">
        @if($errors->has('announcement_date'))
        <p class="text-danger text-sm">{{$errors->first('announcement_date')}}</p>
        @endif
    </div>
    <div class="form-group">
        <label for="body">Isi berita</label>
        <input id="body" type="hidden" name="body" value="{{isset($announcement)? $announcement->body:''}}">
        <trix-editor input="body"></trix-editor>
        @if($errors->has('body'))
        <p class="text-danger text-sm">{{$errors->first('body')}}</p>
        @endif
    </div>
    <div class="form-group">
        <label for="image">Image</label>
        @if(isset($announcement))
        <img class="img-fluid rounded my-4" src="/storage/{{$announcement->image}}" alt="img" style="height:300px; width:400px">
        @endif
        <input type="file" name="image" id="image" class="form-control">
        @if($errors->has('image'))
        <p class="text-danger text-sm">{{$errors->first('image')}}</p>
        @endif
    </div>
    <div class="form-group">
        <label for="category">Category</label>
        <select name="category" id="category" class="form-control">
            @foreach($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
        </select>
        @if($errors->has('category'))
        <p class="text-danger text-sm">{{$errors->first('category')}}</p>
        @endif
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success btn-sm">{{$buttonText}}</button>
    </div>
</div>

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    flatpickr('#announcement_date', {
        enableTime: true,
        enableSeconds: true
    })
</script>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection