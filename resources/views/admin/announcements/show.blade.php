@extends('admin.app')
@section('content')

<div class="row">

    <!-- Post Content Column -->
    <div class="container">
        <h1 class="mt-4 mb-3"> {{$announcement->title}} </h1>
        <!-- Preview Image -->
        <img class="img-fluid rounded" src="/storage/{{$announcement->image}}" alt="">
        <hr>

        <!-- Date/Time -->
        <p class="my-2">{{formatDate($announcement->announcement_date)}}</p>
        <hr>
        <div>{!! $announcement->body !!}</div>
    </div>
</div>
@endsection