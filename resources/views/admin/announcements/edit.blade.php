@extends('admin.app')
@section('content')
<div class="card card-default">
    <div class="card-header">Update Post</div>
    <form action="{{route('announcements.update', $announcement)}}" method="POST" enctype="multipart/form-data">
        @method('PATCH')
        @include('admin.announcements.form', [
        'buttonText' => 'Update Berita'
        ])
    </form>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.js"></script>
@endsection
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.css">
@endsection