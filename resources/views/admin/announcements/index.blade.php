@extends('admin.app')
@section('content')
<?php $ctr = 0; ?>
<!-- Content Wrapper. Contains page content -->
<div class="row">
    <a href="{{route('announcements.create')}}" class="btn btn-success btn-sm m-4">Create Berita</a>
</div>


<div class="container">
    <table class="table table-hover">
        @if($announcements->count())
        <thead class="bg-orange text-white">
            <th>No</th>
            <th>Title</th>
            <th>Tanggal</th>
            <th>Kategory</th>
            <th>Aksi</th>
        </thead>
        <tbody>
            @foreach($announcements as $announcement)
            <?php $ctr++ ?>
            <tr>
                <td>{{$ctr}}</td>
                <td>{{$announcement->title}}</td>
                <td>{{$announcement->announcement_date}}</td>
                <td>{{$announcement->category->name}}</td>
                <td>
                    <div class="row mx-auto">
                        <span>
                            <a href="{{route('announcements.show', $announcement)}}">
                                <i class="nav-icon fas fa-eye "></i>
                            </a>
                        </span>
                        <span class="mx-2">
                            <a href="{{route('announcements.edit', $announcement)}}">
                                <i class="nav-icon fas fa-edit"></i>
                            </a>
                        </span>
                        <form action="{{route('announcements.destroy', $announcement)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <span>
                                <button class="border-0 bg-light" type="submit">
                                    <i class="nav-icon fas fa-trash text-danger"></i>
                                </button>
                        </form>

                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
        @else
        <h4>
            <center>Belum ada Berita/Kegiatan</center>
        </h4>
        @endif
    </table>
</div>
@endsection