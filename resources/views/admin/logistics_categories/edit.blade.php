@extends('admin.app')
@section('content')
<div class="container">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Membuat Kecamatan Baru</div>
            <div class="card-body">
                <form action="{{route('logistics-categories.update', $logisticsCategory)}}" method="POST">
                    @method('PATCH')
                    @include('admin.logistics_categories.form',[
                    'buttonText' => 'Edit'
                    ])
                </form>
            </div>
        </div>
    </div>
</div>
@endsection