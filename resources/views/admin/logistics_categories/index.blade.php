@extends('admin.app')
@section('content')
<?php $no = 0; ?>
<div class="container">
    <div class="row">
        <a href="{{route('logistics-categories.create')}}" class="btn btn-success my-4 ml-3">Tambah Jenis Barang Logistik</a>
    </div>
    <div class="row">
        <div class="col-md-10 mx-auto">

            @if($logisticsCategories->count())
            <table class="table bg-light">
                <thead class="table-primary">
                    <tr>
                        <th scope="col-sm-1">#</th>
                        <th scope="col-sm-5">Name</th>
                        <th scope="col-sm-5">
                            <center>Action</center>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($logisticsCategories as $logisticsCategory)

                    <tr>
                        <?php $no++ ?>
                        <td scope="row border-0">{{$no}}</td>
                        <td><a href="{{route('villages.index', $logisticsCategory)}}">{{$logisticsCategory->name}}</a></td>
                        <td class="d-flex justify-content-center ">
                            <div class="row">
                                <span>
                                    <a href="{{route('logistics-categories.edit', $logisticsCategory)}}">
                                        <i class="nav-icon fas fa-eye text-info"></i>
                                    </a>
                                </span>
                                <form action="{{route('logistics-categories.destroy', $logisticsCategory)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <span class="">
                                        <button class="border-0 bg-light" type="submit">
                                            <i class="nav-icon fas fa-trash text-danger"></i>
                                        </button>
                                </form>

                            </div>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <h3>
                <center>Belum ada Jenis Barang</center>
            </h3>
            @endif
        </div>
    </div>
</div>
@endsection