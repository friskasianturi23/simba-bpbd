  @csrf
  <div class="form-group">
      <label for="name">Nama Kategory</label>
      <input type="text" name="name" id="name" class="form-control" value="{{$logisticsCategory->name}}">
      @if($errors->has('name'))
      <div class="text-danger text-sm">{{ $errors->first('name') }}</div>
      @endif
  </div>
  <div class="form-group">
      <button class="btn btn-success" type="submit">{{$buttonText}}</button>
  </div>