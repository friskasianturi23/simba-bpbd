@extends('admin.app')
@section('content')
<div class="container">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Membuat Jenis Barang Logistik Baru</div>
            <div class="card-body">
                <form action="{{route('logistics-categories.store')}}" method="POST">
                    @include('admin.logistics_categories.form',[
                    'buttonText' => 'Create',
                    'logisticsCategory' => new App\LogisticsCategory
                    ])
                </form>
            </div>
        </div>
    </div>
</div>
@endsection