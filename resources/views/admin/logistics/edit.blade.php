 @extends('admin.app')
 @section('content')
 <div class="col-md-8 mx-auto">
   <div class="card card-default">
     <div class="card-header">Tambah Data Logistik</div>
     <div class="card-body">
       <form action="{{route('logistics.update', $logistic)}}" method="POST">
         @method('PATCH')
         @csrf
         <div class="form-group">
           <label for="nama_barang">Nama Barang</label>
           <input id="nama_barang" type="text" class="form-control" name="nama_barang" value="{{$logistic->nama_barang}}">
           @if($errors->has('nama_barang'))
           <div class="text-danger text-sm">{{ $errors->first('nama_barang') }}</div>
           @endif
         </div>

         <div class="form-group">
           <label for="logisticscategory_id"> Jenis barang logistik</label>
           <select class="form-control" name="logisticscategory_id">
             <option>--Pilih Jenis Paket Logistik--</option>
             <!-- Masih salah belum masuk ke database -->
             @foreach($jenispaketlogistiks as $jenispaket)
             <option value="{{$jenispaket->id}}" @if($jenispaket->id ==$logistic->logisticscategory->id)
               selected
               @endif
               > {{$jenispaket->name}} </option>
             @endforeach
           </select>
           @if($errors->has('logistics_category_id'))
           <div class="text-danger text-sm">{{$errors->first('logistics_category_id')}}</div>
           @endif
         </div>

         <div class="form-group">
           <label for="jumlah_ketersediaan">Jumlah Ketersedian</label>
           <input name="jumlah_ketersediaan" class="form-control" type="number" value="{{$logistic->jumlah_ketersediaan}}">
           @if($errors->has('jumlah_ketersediaan'))
           <div class="text-danger text-sm">{{$errors->first('jumlah_ketersediaan')}}</div>
           @endif
         </div>

         <div class="form-group">
           <label for="instansi">Instansi</label>
           <select class="form-control" name="instansi">
             <option @if($logistic->instansi === "Pencegahan dan Kesiapsiagaan")
               selected @endif
               >Pencegahan dan Kesiapsiagaan</option>
             <option @if($logistic->instansi === "Kedaruratan dan Logistik")
               selected @endif>
               Kedaruratan dan Logistik</option>
             <option @if($logistic->instansi === "Rehabilitasi dan Rekonstruksi")
               selected @endif>
               Rehabilitasi dan Rekonstruksi</option>
           </select>
           @if($errors->has('instansi'))
           <div class="text-danger text-sm">{{$errors->first('instansi')}}</div>
           @endif
         </div>

         <div class="form-group">
           <label for="kondisi">Kondisi</label>
           <div class="row">
             <div class="col-md-3">
               <input type="radio" name="kondisi" value="Bagus" @if ($logistic->kondisi == "Bagus") checked @endif /> Bagus
             </div>
             <div class="col-md-3"> <input type="radio" name="kondisi" value="Rusak" @if ($logistic->kondisi == "Rusak") checked @endif /> Rusak
             </div>
           </div>
           @if($errors->has('kondisi'))
           <div class="text-danger text-sm">{{$errors->first('kondisi')}}</div>
           @endif
         </div>

         <div class="form-group">
           <label for="tanggal_diterimanya_barang">Tanggal Diterima</label>
           <input type="date" class="form-control" name="tanggal_diterimanya_barang" value="{{ $logistic->tanggal_diterimanya_barang  }}" autofocus>
           @if($errors->has('tanggal_diterimanya_barang'))
           <div class="text-danger text-sm">{{$errors->first('tanggal_diterimanya_barang')}}</div>
           @endif
         </div>

         <div class="form-group">
           <label for="nomor_surat_tanggal_penyerahan">Nomor Surat Penyerahan</label>
           <input type="text" class="form-control @error('name') is-invalid @enderror" name="nomor_surat_tanggal_penyerahan" value=" {{$logistic->nomor_surat_tanggal_penyerahan }}" autocomplete="nomor_surat_tanggal_penyerahan">
           @if($errors->has('nomor_surat_tanggal_penyerahan'))
           <div class="text-danger text-sm">{{$errors->first('nomor_surat_tanggal_penyerahan')}}</div>
           @endif
         </div>

         <div class="form-group">
           <label for="tanggal_kadaluarsa_barang">Tanggal Kadaluarsa</label>
           <input type="date" class="form-control" name="tanggal_kadaluarsa_barang" value="{{  $logistic->tanggal_kadaluarsa_barang  }}">
           <em>*Tanggal kadaluarsa dapat dikosongkan</em>

         </div>

         <div class="form-group">
           <label for="tahun_perolehan">Tahun Perolehan</label>
           <input type="text" class="form-control @error('name') is-invalid @enderror" name="tahun_perolehan" value="{{  $logistic->tahun_perolehan  }}" autocomplete="tahun_perolehan">
           @if($errors->has('tahun_perolehan'))
           <div class="text-danger text-sm">{{$errors->first('tahun_perolehan')}}</div>
           @endif
         </div>

         <div class="form-group">
           <button class="btn btn-success" type="submit">Submit</button>
         </div>
       </form>
     </div>
   </div>
 </div>
 @endsection