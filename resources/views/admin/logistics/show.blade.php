@extends('admin.app')
@section('content')
<div class="card my-2">
  <div class="card-header">Data Logistik</div>
  <div class="card-body">
    <div class="col-md-12">
      <br>

      <table class="table">
        <tr>
          <td>Nama barang</td>
          <td>:</td>
          <td>{{$logistic->nama_barang}}</td>
        </tr>
        <tr>
          <td>Jenis Kategory barang</td>
          <td>:</td>
          <td>{{$logistic->logisticscategory->name}}</td>
        </tr>
        <tr>
          <td>Jumlah Ketersediaan</td>
          <td>:</td>
          <td>{{$logistic->jumlah_ketersediaan}}</td>
        </tr>
        <tr>
          <td>Instansi</td>
          <td>:</td>
          <td>{{$logistic->instansi}}</td>
        </tr>
        <tr>
          <td>Kondisi Barang</td>
          <td>:</td>
          <td>{{$logistic->kondisi }}</td>
        </tr>
        <tr>
          <td>Nomor Surat Tanggal Penyerahan</td>
          <td>:</td>
          <td>{{$logistic->nomor_surat_tanggal_penyerahan}}</td>
        </tr>
        <tr>
          <td>Tahun Perolehan</td>
          <td>:</td>
          <td>{{$logistic->tahun_perolehan}}</td>
        </tr>
        <tr>
          <td>Tanggal Penerimaan barang</td>
          <td>:</td>
          <td>{{$logistic->tanggal_diterimanya_barang}}</td>
        </tr>
        <tr>
          <td>Tanggal Kadaluarsa Barang</td>
          <td>:</td>
          <td>{{$logistic->tanggal_kadaluarsa_barang}}</td>
        </tr>

      </table>
      <br>
    </div>
  </div>
</div>
@endsection