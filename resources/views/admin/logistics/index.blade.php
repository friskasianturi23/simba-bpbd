@extends('admin.app')
@section('content')
<div class="card my-2">
  <div class="card-header">
    <h2>{{ __('Logistik') }}</h2>
  </div>
  <div class="card-body">
    <div class="col-md-12">
      <br>
      <a href="{{ route('logistics.create')}}" class="btn btn-success">Tambah Data Logistik</a>
      <br></br>

      <div class="col-md-4">
        <form action="{{route('logistics.search')}}" method="get">
          <div class="input-group">
            <input type="search" name="q" class="form-control" placeholder="Cari Data .." value="">
            <span class="input-group-prepend">
              <input type="submit" class="btn btn-primary" value="CARI">
            </span>
          </div>
        </form>
      </div>
      @if ($logistiks->count() !== 0)

      <table class="table">
        <thead>
          <th>No</th>
          <th>Nama Barang</th>
          <th>Jenis Barang</th>
          <th>Jumlah Persediaan</th>
          <th>Tahun Penerimaan</th>
          <th colspan="3">Opsi</th>
        </thead>

        @php
        $i = 1;
        @endphp
        @foreach($logistiks as $logistic)
        <tr>
          <td>{{$i}}</td>
          <td>{{$logistic->nama_barang}}</td>
          <td>{{$logistic->logisticscategory->name}}</td>
          <td>{{$logistic->jumlah_ketersediaan}}</td>
          <td>{{$logistic->tahun_perolehan}}</td>
          <!-- <td>
            <div class="d-flex">
              <a href="{{route('logistics.show', $logistic)}}" class="btn btn-primary m-2">Detail</a>

              <form action="{{ route('logistics.destroy', $logistic) }}" method="POST">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button class="btn btn-danger m-2" onclick="return confirm('Yakin ingin menghapus data?');">Hapus</button>
              </form>
            </div>
          </td> -->
          <td>
            <div class="row">
              <span>
                <a href="{{route('logistics.show', $logistic)}}">
                  <i class="nav-icon fas fa-eye text-info"></i>
                </a>
              </span>
              <span class="mx-4">
                <a href="{{route('logistics.edit', $logistic)}}">
                  <i class="nav-icon fas fa-edit"></i>
                </a>
              </span>
              <form action="{{route('logistics.destroy', $logistic)}}" method="POST">
                @csrf
                @method('DELETE')
                <span class="">
                  <button class="border-0 bg-light" type="submit">
                    <i class="nav-icon fas fa-trash text-danger"></i>
                  </button>
              </form>
            </div>
          </td>
        </tr>
        @php
        $i++;
        @endphp
        @endforeach
        </br>
        <script>
        </script>
      </table>
      @else
      <br>
      <hr>
      <h5>Data Logistik belum ditambahkan.</h5>
      @endif
      {{$logistiks->links()}}
    </div>
  </div>
</div>

@endsection