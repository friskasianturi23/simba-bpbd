 @extends('admin.app')
 @section('content')
 <div class="card">
     <div class="card-header">Kategori</div>
     <div class="card-body">
         <a href="{{route('categories.create')}}" class="btn btn-success btn-sm m-2">Tambah Kategory</a>
         @if($categories->count())
         <table class="table">
             <thead>
                 <th>Nama</th>
                 <th>Aksi</th>
                 <th></th>
             </thead>
             <tbody>
                 @foreach($categories as $category)
                 <tr>
                     <h5>
                         <td> {{$category->name}}</td>
                         <td>
                             <div class="row">
                                 <span class="">
                                     <a href="{{route('categories.edit', $category)}}">
                                         <i class="nav-icon fas fa-edit"></i>
                                     </a>
                                 </span>
                                 <form action="{{route('categories.destroy', $category)}}" method="POST">
                                     @csrf
                                     @method('DELETE')
                                     <span class="mx-4">
                                         <button class="border-0 bg-light" type="submit">
                                             <i class="nav-icon fas fa-trash text-danger"></i>
                                         </button>
                                 </form>
                             </div>
                         </td>
                     </h5>
                 </tr>
                 @endforeach
             </tbody>
         </table>
         @else
         <center>Belum ada kategory Berita</center>
         @endif

     </div>
 </div>
 @endsection