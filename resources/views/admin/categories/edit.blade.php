@extends('admin.app')
@section('content')
<div class="col-md-8">
    <div class="card card-default">
        <div class="card-header">Update Category</div>
        <form action="{{route('categories.update', $category)}}" method="POST">
            @method('PATCH')
            @include('admin.categories.form',[
            'buttonText' => 'Update Category'])
        </form>
    </div>
</div>
@endsection