@csrf
<div class="card-body">
    <div class="form-group">
        <label for="name">Jenis Kategory Berita</label>
        <input type="text" name="name" id="name" class="form-control" value="{{$category->name}}">
    </div>
    <div class="form-group">
        <button class="btn btn-success" type="submit">{{$buttonText}}</button>
    </div>
</div>