@extends('admin.app')
@section('content')
<div class="col-md-4"></div>
<div class="col-md-8 items-center">
    <div class="card card-default">
        <div class="card-header">Create Category</div>
        <form action="{{route('categories.store')}}" method="POST">
            @include('admin.categories.form',[
            'buttonText' => 'Create Category',
            'category' => new App\Category
            ]);
        </form>
        @include('partials.errors')
    </div>
</div>
@endsection