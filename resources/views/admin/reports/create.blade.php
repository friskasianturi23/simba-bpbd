@extends('admin.app')
@section('content')
<div class="container">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Buat laporan baru</div>
            <div class="card-body">
                <form action="{{route('reports.store')}}" method="POST" enctype="multipart/form-data">

                    @include('admin.reports.form',[
                    'buttonText' => 'Buat Laporan'
                    ])

                </form>
            </div>
        </div>
    </div>
</div>
@endsection
<!-- @section('scripts')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    flatpickr('#incident_time', {
        enableTime: true,
        enableSeconds: true
    })
</script>
@endsection
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection -->