@extends('admin.app')
@section('content')

<div class="row">
    <!-- Post Content Column -->
    <div class="col-lg-8">
        @if(!$report->validated)
        <div class="row">
            <a href="{{route('reports.validate', $report)}}" class="btn btn-info text-white">Validasi Laporan</a>
        </div>
        @endif
        <h1 class="mt-4 mb-3">Laporan {{$report->disaster->name}}</h1>
        <!-- Date/Time -->
        <p class="my-2 text-primary">{{formatDate($report->incident_time)}}</p>
        <hr>
        <!-- Preview Image -->
        <img class="img-fluid rounded" src="/storage/{{$report->image}}" alt="">
        <hr>

        <div class="row">
            <p>Location: {{$report->street}}, {{$report->village->name}}, {{$report->village->subdistrict->name}}</p>
        </div>
        <div class="row">
            <p>Pelapor: {{$report->name}}</p>
        </div>
        <div class="row">
            <p>Nomor telepon pelapor : {{$report->phone_number}}</p>
        </div>
        <div class="row">
            <p>Status kepemilikan : {{$report->ownership}}</p>
        </div>
        <div class="row">
            <p>{{ $report->description }}</p>
        </div>
    </div>
</div>
@endsection