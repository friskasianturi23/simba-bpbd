@csrf
<div class="form-group">
    <label for="name">Nama Anda</label>
    <input type="text" name="name" id="name" class="form-control">
    @if($errors->has('name'))
    <div class="text-danger text-sm">{{ $errors->first('name') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="phone_number">Nomor telepon</label>
    <input type="text" name="phone_number" id="phone_number" class="form-control">
    @if($errors->has('phone_number'))
    <div class="text-danger text-sm">{{ $errors->first('phone_number') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="disaster">Jenis bencana yang terjadi</label>
    <select name="disaster" id="disaster" class="form-control">
        @forelse($disasters as $disaster)
        <option value="{{$disaster->id}}">{{$disaster->name}}</option>
        @empty
        Belum ada pilihan bencana
        @endforelse
    </select>
    @if($errors->has('disaster'))
    <div class="text-danger text-sm">{{ $errors->first('disaster') }}</div>
    @endif
</div>

<div class="form-group">
    <label for="incident_time">Waktu kejadian </label>
    <input type="text" name="incident_time" id="incident_time" class="form-control">
    @if($errors->has('incident_time'))
    <div class="text-danger text-sm">{{ $errors->first('incident_time') }}</div>
    @endif
</div>

<div class="form-group">
    <label for="description">Deskripsi Kejadian</label>
    <textarea name="description" id="description" cols="30" rows="5" class="form-control"></textarea>
    @if($errors->has('description'))
    <div class="text-danger text-sm">{{ $errors->first('description') }}</div>
    @endif
</div>

<div class="form-group">
    <label for="ownership">Kepemilikan</label>
    <input type="text" name="ownership" id="ownership" class="form-control" placeholder="Contoh : Milik sendiri, kontrakan ">
    @if($errors->has('ownership'))
    <div class="text-danger text-sm">{{ $errors->first('ownership') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="kecamatan">Kecamatan</label>
    <select class="form-control" id="subs_id" name="kecamatan">
        @foreach($subdistricts as $subdistrict)
        <option value="{{$subdistrict->id}}">{{$subdistrict->name}}</option>
        @endforeach
    </select>
    @if($errors->has('kecamatan'))
    <div class="text-danger text-sm">{{ $errors->first('kecamatan') }}</div>
    @endif
    <label for="village">Desa</label>
    <select id="villagesname" name="village" class="form-control">
        <option value="0" disabled="true" selected="true">Desa/Kelurahan</option>
    </select>
    @if($errors->has('village'))
    <div class="text-danger text-sm">{{ $errors->first('village') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="street">Jalan</label>
    <input type="text" name="street" id="street" class="form-control">
    @if($errors->has('street'))
    <div class="text-danger text-sm">{{ $errors->first('street') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="image">Gambar</label>
    <input type="file" name="image" id="image" class="form-control-file">
    @if($errors->has('image'))
    <div class="text-danger text-sm">{{ $errors->first('image') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="image">NIK</label>
    <input type="number" name="nik" id="nik" class="form-control">
</div>
<div class="form-group">
    <button class="btn btn-success" type="submit">{{$buttonText}}</button>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#subs_id').change(function() {
            //  var $village = $('#city_id');
            var op = "";

            var select = $(this).parent(); //Cari tag html yang select

            $.ajax({
                type: 'get', //Type of request
                url: "/findVillages",
                data: {
                    'subs_id': $(this).val()
                },
                success: function(data) {
                    // console.log('success');
                    console.log(data);

                    op += '<option value="0" selected disabled>Pilih Desa/Kelurahan</option>';

                    for (var i = 0; i < data.length; i++) {
                        op += '<option value=" ' + data[i].id + ' ">' + data[i].name + '</option>';
                    }
                    select.find('#villagesname').html(" ");
                    select.find('#villagesname').append(op);

                },
                error: function() {
                    console.log("Does not work");
                }
            });
        });
    });
</script>

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    flatpickr('#incident_time', {
        enableTime: true,
        enableSeconds: true
    })
</script>
@endsection
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection