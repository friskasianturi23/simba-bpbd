@extends('admin.app')
@section('content')
<div class="container">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Membuat Desa/Kelurahan Baru</div>
            <div class="card-body">
                <form action="{{route('villages.store', $subdistrict)}}" method="POST">
                    @include('admin.reports.villages.form',[
                    'buttonText' => 'Create',
                    'village' => new App\Village
                    ])
                </form>
                @include('partials.errors')
            </div>
        </div>
    </div>
</div>
@endsection