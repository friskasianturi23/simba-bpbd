@extends('admin.app')
@section('content')
<?php $no = 0; ?>

<div class="container">
    <div class="row">
        <h3>Kecamatan {{$subdistrict->name}}</h3>
    </div>
    <div class="row">
        <a class="btn btn-success my-4 ml-3 btn-sm" href="{{route('villages.create', $subdistrict)}}">Buat Desa/Kelurahan</a>
    </div>
    <table class="table bg-light">
        <thead class="table-primary">
            <tr>
                <th scope="col-sm-1">#</th>
                <th scope="col-sm-3">Name</th>
                <th scope="col-sm-3">
                    <center>Action</center>
                </th>
            </tr>
        </thead>
        <tbody>
            @if($subdistrict->villages->count() > 0)
            @foreach($subdistrict->villages as $village)

            <tr>
                <?php $no++ ?>
                <td scope="row border-0">{{$no}}</td>
                <td>{{$village->name}}</td>
                <td class="d-flex justify-content-center ">
                    <span class="">
                        <a href="{{route('villages.edit', [$subdistrict,$village])}}">
                            <i class="nav-icon fas fa-edit"></i>
                        </a>
                    </span>
                    <form action="{{route('villages.destroy', [$subdistrict,$village])}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <span class="mx-4">
                            <button class="border-0 bg-light" type="submit">
                                <i class="nav-icon fas fa-trash text-danger"></i>
                            </button>
                    </form>

                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td>
                    <center>
                        <h3>Belum Ada Desa/Kelurahan</h3>
                    </center>
                </td>
            </tr>
            @endif
        </tbody>
    </table>
    @endsection