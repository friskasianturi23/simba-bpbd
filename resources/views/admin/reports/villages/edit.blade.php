@extends('admin.app')
@section('content')
<div class="container">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Edit Desa/Kelurahan</div>
            <div class="card-body">
                <form action="{{route('villages.update', [$village, $subdistrict])}}" method="POST">
                    @method('PATCH')
                    @include('admin.reports.villages.form',[
                    'buttonText' => 'Edit'
                    ])
                </form>
                @include('partials.errors')
            </div>
        </div>
    </div>
</div>
@endsection