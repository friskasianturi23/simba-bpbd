  @csrf
  <div class="form-group">
      <label for="name">Nama Desa/Kelurahan</label>
      <input type="text" name="name" id="name" class="form-control" value="{{$village->name}}">
  </div>
  <div class="form-group">
      <button class="btn btn-success" type="submit">{{$buttonText}}</button>
  </div>