@extends('admin.app')
@section('content')
<?php $ctr = 0; ?>
<div class="row mb-2">
    <a href="{{route('reports.create')}}" class="btn btn-success">Buat laporan</a>
</div>
<div class="container">
    <table class="table table-bordered table-hover">
        @if($reports->count())
        <thead class="bg-orange text-white">
            <th>No</th>
            <th>Nama bencana</th>
            <th>Tanggal Kejadian</th>
            <th>Lokasi</th>
            <th>Nama</th>
            <th>No telepon pelapor</th>
            <th>Aksi</th>
        </thead>
        <tbody>
            @foreach($reports as $report)
            <?php $ctr++; ?>
            <tr>
                <td>{{$ctr}}</td>
                <td>{{$report->disaster->name}}</td>
                <td>{{$report->incident_time}}</td>
                <td>{{$report->street}}, {{$report->village->name}}</td>
                <td>{{$report->name}}</td>
                <td>{{$report->phone_number}}</td>
                <td>
                    <span class="mr-2">
                        <a href="{{route('reports.show', $report)}}">
                            <i class="nav-icon fas fa-eye mr-2"></i>
                        </a>
                    </span>
                    <span class="">
                        <a href="#">
                            <i class="nav-icon fas fa-check {{($report->validated) ? 'text-success':'text-danger' }}"></i>

                        </a>
                    </span>

                </td>
            </tr>
            @endforeach
        </tbody>
        @else
        <h4>
            <center>Belum ada Laporan</center>
        </h4>
        @endif
    </table>
</div>
@endsection