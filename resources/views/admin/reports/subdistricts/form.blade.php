  @csrf
  <div class="form-group">
      <label for="name">Nama Kecamatan</label>
      <input type="text" name="name" id="name" class="form-control" value="{{$subdistrict->name}}">
      @if($errors->has('name'))
      <p class="text-danger text-sm">{{$errors->first('name')}}</p>
      @endif
  </div>
  <div class="form-group">
      <button class="btn btn-success" type="submit">{{$buttonText}}</button>
  </div>