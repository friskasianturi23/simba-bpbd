@extends('admin.app')
@section('content')
<div class="container">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Membuat Kecamatan Baru</div>
            <div class="card-body">
                <form action="{{route('subdistricts.store')}}" method="POST">
                    @include('admin.reports.subdistricts.form',[
                    'buttonText' => 'Create',
                    'subdistrict' => New App\Subdistrict
                    ])
                </form>
                @include('partials.errors')
            </div>
        </div>
    </div>
</div>
@endsection