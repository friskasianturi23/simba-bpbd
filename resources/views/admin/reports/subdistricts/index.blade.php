@extends('admin.app')
@section('content')
<?php $no = 0; ?>
<div class="container">
    <div class="row">
        <a href="{{route('subdistricts.create')}}" class="btn btn-success my-4 ml-3">Tambah Kecamatan</a>
    </div>
    @if($subdistricts->count())
    <table class="table bg-light">
        <thead class="table-primary">
            <tr>
                <th scope="col-sm-1">#</th>
                <th scope="col-sm-5">Name</th>
                <th scope="col-sm-5">
                    <center>Action</center>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($subdistricts as $subdistrict)

            <tr>
                <?php $no++ ?>
                <td scope="row border-0">{{$no}}</td>
                <td><a href="{{route('villages.index', $subdistrict)}}">{{$subdistrict->name}}</a></td>
                <td class="d-flex justify-content-center ">
                    <span class="">
                        <a href="{{route('subdistricts.edit', $subdistrict)}}">
                            <i class="nav-icon fas fa-edit"></i>
                        </a>
                    </span>
                    <form action="{{route('subdistricts.destroy', $subdistrict)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <span class="mx-4">
                            <button class="border-0 bg-light" type="submit">
                                <i class="nav-icon fas fa-trash text-danger"></i>
                            </button>
                    </form>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <h3>
        <center>Belum ada Kecamatan</center>
    </h3>
    @endif
</div>
@endsection