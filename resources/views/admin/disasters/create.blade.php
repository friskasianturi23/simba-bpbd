@extends('admin.app')
@section('content')
<div class="card card-default">
    <div class="card-header">Create Disaster</div>
    <div class="card-body">
        <form action="{{route('disasters.store')}}" method="POST">
            @include('admin.disasters.form', [
            'buttonText' => 'Create',
            'disaster' => new App\Disaster
            ])

        </form>
        @include('partials.errors')
    </div>
</div>
@endsection