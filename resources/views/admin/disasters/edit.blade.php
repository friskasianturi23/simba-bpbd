@extends('admin.app')
@section('content')
<div class="card card-default">
    <div class="card-header">Create Disaster</div>
    <div class="card-body">
        <form action="{{route('disasters.update', $disaster)}}" method="POST">
            @method('patch')
            @include('admin.disasters.form', [
            'buttonText' => 'Update'
            ])
        </form>
        @include('partials.errors')
    </div>
</div>
@endsection