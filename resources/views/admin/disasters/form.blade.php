@csrf
<div class="form-group">
    <label for="name">Nama Bencana</label>
    <input type="text" name="name" id="name" class="form-control" value="{{$disaster->name}}">
</div>
<div class="form-group">
    <label for="description">Description</label>
    <textarea name="description" id="description" cols="30" rows="5" class="form-control">{{$disaster->description}}</textarea>
</div>
<button type="submit" class="btn btn-success">{{$buttonText}}</button>