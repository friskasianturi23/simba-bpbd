@extends('admin.app')
@section('content')
<?php $ctr = 0; ?>
<div class="row">
    <a href="{{route('disasters.create')}}" class="btn btn-success mb-3">Tambah jenis bencana</a>
</div>
<h5 class="mb-2">Kategory Bencana</h5>
<div class="container">
    <table class="table table-bordered table-hover">
        @if($disasters->count())
        <thead class="bg-orange text-white">
            <th>No</th>
            <th>Name</th>
            <th>Deskripsi</th>
            <th>Aksi</th>
        </thead>
        <tbody>
            @foreach($disasters as $disaster)
            <?php $ctr++ ?>
            <tr>
                <td>{{$ctr}}</td>
                <td>{{$disaster->name}}</td>
                <td>{{$disaster->description}}</td>

                <td>
                    <div class="row mx-auto">
                        <span class="">
                            <a href="{{route('disasters.edit', $disaster)}}">
                                <i class="nav-icon fas fa-edit"></i>
                            </a>
                        </span>
                        <form action="{{route('disasters.destroy', $disaster)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <span class="mx-4">
                                <button class="border-0 bg-light" type="submit">
                                    <i class="nav-icon fas fa-trash text-danger"></i>
                                </button>
                        </form>
                    </div>

                </td>
            </tr>
            @endforeach
        </tbody>
        @else
        <h4>
            <center>Belum ada jenis bencana</center>
        </h4>
        @endif
    </table>
</div>
@endsection