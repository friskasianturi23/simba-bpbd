@extends('layouts.app')

@section('title')
<title>Tujuan dan Fungsi | BPBD Kota Tebing Tinggi</title>
@endsection

@section('content')
<br>
<section class="container ">
    <!-- <div class="row"> -->
    <div class="col-12 " aria-label="breadcrumb">

        <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="#">Beranda</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">Tujuan dan Fungsi</li>
        </ol>
    </div>

</section>

<div class="container ">
    <center>
        <h5><strong> Sekilas Tentang BPBD Tebing Tinggi</strong></h5>
        <p>

            Badan Penanggulangan Bencana Daerah (BPBD) merupakan lembaga penanggulangan
            bencana yang berkedudukan di bawah dan bertanggung jawab kepada Gubernur.
            BPBD dipimpin oleh seorang kepala, yang dijabat secara ex officio oleh Sekretaris Daerah (Sekda),
            yang berkedudukan di bawah dan bertanggung jawab kepada Gubernur.
        </p>
    </center>

    <h5>
        <strong>
            Jenis Pelayanan
        </strong>
    </h5>
    <p>
        <ol>
            <li>Pelayanan Pemberian Bantuan Pemenuhan Kebutuhan SARPRAS Penanggulangan Bencana</li>
            <li>Pelayanan Pemberian Bantuan Pemenuhan Kebutuhan Dasar kepada Korban Bencana</li>
            <li>Pelayanan Kaji Tepat & Cepat Penanganan Darurat Bencana</li>
        </ol>
    </p>
</div>



<br><br><br>
@endsection