@extends('layouts.app')

@section('title')

<title>Visi dan Misi | BPBD Kota Tebing Tinggi</title>
@endsection

@section('content')
<br>
<section class="container ">
    <!-- <div class="row"> -->
    <div class="col-12 " aria-label="breadcrumb">

        <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="#">Beranda</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">Visi dan Misi</li>
        </ol>
    </div>

</section>

<div class="container ">
    <!-- <center> -->
    <center>
        <h3>
            <strong>Visi</strong>
        </h3>
        <h5>
            “Ketangguhan masyarakat dalan menghadapi bencana”
        </h5>
        <h3>
            <strong>
                Misi
            </strong>
        </h3>
        <h5>
            “Meningkatkan kualitas sarana dan prasarana perkotaan”
        </h5>
    </center>
</div>



<br><br><br>

@endsection