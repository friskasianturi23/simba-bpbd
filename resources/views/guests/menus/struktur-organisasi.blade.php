@extends('layouts.app')

@section('title')
<title>Struktur Organisasi| BPBD Kota Tebing Tinggi</title>
@endsection

@section('content')
<br>
<section class="container ">
    <!-- <div class="row"> -->
    <div class="col-12 " aria-label="breadcrumb">

        <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="#">Beranda</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">Struktur Organisasi</li>
        </ol>
    </div>

</section>

<div class="container ">
    <!-- <center> -->
    <div class="col-md-12 mx-auto">
        <div class="row">
            <div class="col-6 mx-auto">
                <img src="{{asset('img/struktur.jpeg')}}" alt="Struktur Oganisasi" class="img-fluid">
            </div>
        </div>
    </div>
</div>
@endsection