@extends('layouts.app')
@section('title')
<title>Beranda | BPBD Kota Tebing Tinggi</title>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="../img/gambar_1.jpg" class="d-block w-100" alt="..." height="500">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>First slide label</h5>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="../img/gambar_2.jpg" class="d-block w-100" alt="..." height="500">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Second slide label</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="../img/gambar_3.jpg" class="d-block w-100" alt="..." height="500">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Third slide label</h5>
                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!-- </div> -->
</div>
<br><br><br>

{{-- menampilkan data cuaca --}}
<?php
$lat = 3.328580;
$lon = 99.152066;

$wilayah = json_decode(file_get_contents("https://ibnux.github.io/BMKG-importer/cuaca/wilayah.json"), true);
$jml = count($wilayah);

//  hitung jarak
for ($n = 0; $n < $jml; $n++) {
    $wilayah[$n]['jarak'] = distance($lat, $lon, $wilayah[$n]['lat'], $wilayah[$n]['lon'], 'K');
}

//urutkan
usort($wilayah, 'urutkanJarak');

$json = json_decode(file_get_contents("https://ibnux.github.io/BMKG-importer/cuaca/" . $wilayah[0]['id'] . ".json"), true);
$time = time();
$n = 0;


?>

<section class="container bg-white" style="border: 2px solid #AEBFFF;">
    <h3>Ramalan Cuaca Kota Tebing Tinggi</h3>
    <hr>
    <div class="row rw">

        <?php
        foreach ($json as $cuaca) {
            $timeCuaca = strtotime($cuaca['jamCuaca']);
            if ($timeCuaca > $time) {
        ?>
                <div class="card cd-size">
                    <img src="https://ibnux.github.io/BMKG-importer/icon/<?= $cuaca['kodeCuaca'] ?>.png">
                    <h5 class="card-text"><?= $cuaca['cuaca'] ?> </h5>
                    <p class="card-text cd-txt"><?= $cuaca['tempC'] ?>&deg;C </p>
                    <p class="card-text cd-txt">Kelembapan : <?= $cuaca['humidity'] ?> %</p>
                    <?php $tanggal = strtotime($cuaca['jamCuaca']);
                    // $pukul = strtotime($cuaca['jamCuaca']);
                    ?>
                    <p class="card-text cd-txt"> <?= date('d M Y ', $tanggal) ?></p>
                    <p class="card-text cd-txt"> <?= date('H:i', $tanggal) ?> WIB</p>

                </div>

        <?php }
        } ?>

    </div>
    <p class="text-secondary font-italic">Sumber : Badan Meteorologi Klimatologi dan Geofisika</p>

</section>

<!-- method untuk mengambil jarak terdekat -->
<?php
// method untuk mengambil jarak
function urutkanJarak($a, $b)
{
    return $a['jarak'] - $b['jarak'];
}


// https://www.geodatasource.com/developers/php
function distance($lat1, $lon1, $lat2, $lon2, $unit)
{
    if (($lat1 == $lat2) && ($lon1 == $lon2)) {
        return 0;
    } else {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}

?>

<br><br><br>
<!-- menampilkan berita  -->
<div class="container my-5" id="berita">
    <div class="row">
        <div class="col-lg-8">
            <div class="col-lg-10 text-center">
                <h2><b>Berita</b>
                </h2>

            </div>
            <table class="table">
                <tbody>
                    @forelse($announcements as $announcement)

                    <tr>
                        <td>
                            <div class="box">
                                <div class="box-body">
                                    <div class="box box-danger" class="link-decoration">
                                        <p><img width="200" height="150" class="img-responsive rounded my-auto" src="/storage/{{$announcement->image}}"></p>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <h5> <strong>{{$announcement->title}}</strong> </h5>
                            <p>{{formatDate($announcement->announcement_date)}}</p>
                            <p style="opacity:0.8">
                                {!!\Illuminate\Support\Str::limit($announcement->body, 200, $end='...') !!}
                                <a href="{{route('guest_news.show', $announcement)}}">&nbsp; Read more</a>
                            </p>
                        </td>
                    </tr>

                    @empty
                    <h5>Belum ada berita</h5>
                    @endforelse
                </tbody>
            </table>
            <center>
                <strong>
                    <a href="{{route('guests.news')}}">Lebih banyak lagi....</a>
                </strong>
            </center>
        </div>
        <div class="col-lg-4">
            <div class="card text-black ">
                <div class="card-header text-white bg-primary">Kegiatan</div>
                <div class="card-body">
                    @forelse($activities as $activity)
                    <a href="{{route('guest_activity.show', $activity)}}" class="link-decoration">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>
                                        <img width="100" height="80" src="/storage/{{$activity->image}}" class="img-responsive rounded my-auto">
                                    </td>
                                    <td>
                                        <p class="card-text">
                                            {{$activity->title}}
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </a>

                    @empty
                    <p>Belum ada berita kegiatan</p>
                    @endforelse

                </div>
                <!-- <hr> -->
                <center class="mb-2">
                    <strong>
                        <a href="{{route('guests.activities')}}">Lebih banyak lagi....</a>
                    </strong>
                </center>
            </div>
        </div>
    </div>
</div>

@endsection