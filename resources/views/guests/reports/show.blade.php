@extends('layouts.app')
@section('title')
<title>Sampaikan Laporan</title>
@endsection
@section('content')
<div class="row">
    <!-- Post Content Column -->
    <div class="col-lg-8 mx-auto">
        <h1 class="mt-4 mb-3">
            <center>Laporan {{$report->disaster->name}}</center>
        </h1>
        <!-- Date/Time -->
        <p class="my-2 text-primary">{{formatDate($report->incident_time)}}</p>
        <hr>
        <!-- Preview Image -->
        <img class="img-fluid rounded" src="/storage/{{$report->image}}" alt="">

        <table class="table">
            <tr>
                <td>Nama pelapor</td>
                <td>:</td>
                <td>{{$report->name}}</td>
            </tr>
            <tr>
                <td>Nik</td>
                <td>:</td>
                <td>{{$report->nik}}</td>
            </tr>
            <tr>
                <td>Lokasi kejadian</td>
                <td>:</td>
                <td>{{$report->street}}, Kel/Desa: {{$report->village->name}}, Kec: {{$report->village->subdistrict->name}}</td>
            </tr>
            <tr>
                <td>Nomor telepon pelapor</td>
                <td>:</td>
                <td>{{$report->phone_number}}</td>
            </tr>
            <tr>
                <td>Status kepemilikan</td>
                <td>:</td>
                <td>{{$report->ownership}}</td>
            </tr>
        </table>
        <strong>Deskripsi kejadian</strong>
        <p>{{ $report->description }}</p>

    </div>
</div>
@endsection