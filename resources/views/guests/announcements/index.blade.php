@extends('layouts.app')
@section('title')
<title>Berita/Kegiatan BPBD</title>
@section
@section('content')
<!-- menampilkan berita  -->
<div class="container my-5">
    <div class="row">
        <div class="col-lg-10 text-center">
            <h2><b>Berita</b>
            </h2>

        </div>
        <table class="table">
            <tbody>
                @forelse($announcements as $announcement)
                <tr>

                    <td>
                        <div class="box">
                            <div class="box-body">
                                <div class="box box-danger" class="link-decoration">
                                    <p><img width="250" height="150" class="img-responsive" src="/storage/{{$announcement->image}}"></p>
                                </div>
                            </div>
                        </div>
                    </td>

                    <td>
                        <a href="{{route('guest_news.show', $announcement)}}">
                            <h5> <strong>{{$announcement->title}}</strong> </h5>
                        </a>
                        <p>{{formatDate($announcement->announcement_date)}}</p>
                        <p style="opacity:0.8">
                            {!!\Illuminate\Support\Str::limit($announcement->body, 300, $end='...') !!}
                        </p>
                    </td>
                </tr>
                @empty
                <h5>Belum ada berita</h5>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection