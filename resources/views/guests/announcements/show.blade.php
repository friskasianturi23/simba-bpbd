@extends('layouts.app')
@section('title')
<title>{{$announcement->title}}</title>
@endsection
@section('content')

<div class="row">

    <!-- Post Content Column -->
    <div class="container">
        <div class="col-md-10 mx-auto mb-5">
            <div class="row">
                <h1 class="mt-4 mb-3"> {{$announcement->title}} </h1>
            </div>
            <!-- Preview Image -->
            <div class="row justify-content-center align-items-center">
                <img class="img-fluid rounded mx-auto" src="/storage/{{$announcement->image}}" alt="Img" style="max-width: 70%; max-height:60%;">
                <hr>
            </div>
            <!-- Date/Time -->
            <p class="my-2">{{formatDate($announcement->announcement_date)}}</p>
            <hr>
            <p>{!! $announcement->body !!}</p>
        </div>
    </div>
</div>
@endsection