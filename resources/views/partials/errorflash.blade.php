<div class="alert alert-success alert-dismissible fade show m-2" role="alert">
  <p class="my-auto"> {{session()->get('error')}}</p>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>