@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md">
            <h3>Kegiatan</h3>
            @foreach($activities as $activity)
            <div class="card card-default mb-4">
                <a href="#"><img class="card-img-top" src="/storage/{{$activity->image}}" alt=""></a>
                <div class="card-body">
                    <h5 class="card-title">
                        <a href="#">{{$activity->title}}</a>
                    </h5>
                    <p class="card-text">{!!\Illuminate\Support\Str::limit($activity->body, 150, $end='...') !!} </p>
                </div>
            </div>
            @endforeach
        </div>
        <div class="col-md">
            <h3>Berita</h3>
            @foreach($announcements as $announcement)
            <div class="card card-default mb-4">
                <a href="#"><img class="card-img-top" src="/storage/{{$activity->image}}" alt=""></a>
                <div class="card-body">
                    <h5 class="card-title">
                        <a href="#">{{$announcement->title}}</a>
                    </h5>
                    <p class="card-text">{!!\Illuminate\Support\Str::limit($announcement->body, 150, $end='...') !!} </p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>


</div>
</div>
@endsection