<!DOCTYPE html>
<html>

<head>
    @yield('title')
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="../img/logo.png">

    @include('layouts.css')

    @yield('css')
</head>
<style>
    html {
        scroll-behavior: smooth;
    }

    .shdw-bg {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        transition: 0.3s;
        border-radius: 5px;
    }

    .cd-size {
        max-width: 18%;
        padding: .75rem;
        margin-bottom: 2rem;
        border: 1px solid #C5C5C5;
        flex-basis: 18%;
        flex-grow: 0;
        flex-shrink: 0;
    }

    .cd-size>img {
        margin-bottom: .75rem;
    }

    .cd-txt {
        font-size: 85%;
    }

    .rw {
        align-items: stretch;
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        overflow-x: auto;
        overflow-y: hidden;
    }
</style>
<!--  -->

<body>
    <header class="act-menu">
        <!-- main navbar -->
        <div class="row navbar-collapse bg-orange" style="height: 90px;">
            <div class="col-1">
            </div>
            <div class=" col-7 navbar navbar-light expand-lg fice white-font-color ">
                <div class="row">
                    <a class=" navbar-brand" href="/">
                        <img src="{{asset('img/logo.png')}}" width="50px" height="50px" alt="">
                        <img src="{{asset('img/logo_pemkot.png')}}" width="50px" height="50px" alt="">
                    </a>
                    <h5 class="text-white my-auto"> Badan Penanggulangan Bencana Daerah <br> Pemerintahan Kota Tebing Tinggi</h5>

                </div>
            </div>
            <div class="col-4">
                <form class="form-inline">
                    <input class="form-control mr-sm-2" type="search" placeholder="Cari" aria-label="Search">

                    <button class="btn btn-default my-2 my-sm-0" type="submit"><i class="fa fa-search"></i></button>
                </form>

            </div>
        </div>

        <div class=" row navbar navbar-expand-md navbar-light menu-color ">
            <div class="col-1">
            </div>
            <div class="col-11">
                <div class="collapse navbar-collapse justify-content-between" id="nav">
                    <ul class="navbar-nav ">
                        <li class="nav-item menu-active ">
                            <a class="nav-link text-dark font-weight-bold px-3 mnu" href="/">Beranda</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark font-weight-bold px-3 mnu" href="{{route('reports.guest.create')}}">Lapor</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link text-dark font-weight-bold px-3 dropdown-toggle  " href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Profil
                            </a>
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownPortfolio">
                                <a class="dropdown-item " href="{{route('visi-misi')}}">Visi dan Misi</a>
                                <a class="dropdown-item " href="{{route('tujuan')}}"> Tujuan dan Fungsi</a>
                                <a class="dropdown-item " href="{{route('struktur-organisasi')}}">Struktur Organisasi</a>

                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark font-weight-bold px-3 mnu" href="#berita">Berita</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark font-weight-bold px-3 mnu" href="#kontak">Kontak</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>



    <!-- Content-->
    @if(session()->has('success'))
    @include('partials.successflash')
    @endif

    @if(session()->has('error'))
    @include('partials.errorflash')
    @endif


    @yield('content')
    <!-- /Content-->



    <!-- Footer -->
    <div class="py-2 bg-red">
        <div class="container py-5 bg-red text-white">
            <div class="row ml-0">
                <p>Hubungi kami</p>
            </div>
            <div class="row">
                <div class="col">
                    <p>BPBD Kota Tebing Tinggi</p>
                    <p>
                        Jl. Gunung Leuseur, Kel.Tj Marulak, Kec. Rambutan, Kota Tebing Tinggi, Sumatera
                        Utara, 20614
                    </p>
                    <p>
                        bpbdtebing@gmail.com
                    </p>
                </div>
                <div class="col">
                    <p>Telp. 0621 2610001</p>
                    <p>Fax. 0621 2610001</p>
                </div>
            </div>
        </div>
    </div>
    <footer class="py-1 bg-dark" id="footer">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; BPBD Tebing Tinggi 2020</p>
        </div>
    </footer>

    <!-- /Footer -->
</body>

@yield('scripts')
@include('layouts.js')

</html>