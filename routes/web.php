<?php

use Illuminate\Routing\Route as RoutingRoute;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//...... Auth route
Auth::routes();


// Home
Route::get('/', 'GuestHomeController@index');

//routing untuk visi dan misi
Route::get('/visi-misi', function () {
    return view('guests.menus.visi-misi');
})->name('visi-misi');

//routing untuk tujuan dan fungsi
Route::get('/tujuan', function () {
    return view('guests.menus.tujuan');
})->name('tujuan');

//routing untuk struktur dan fungsi
Route::get('/struktur-organisasi', function () {
    return view('guests.menus.struktur-organisasi');
})->name('struktur-organisasi');

//routing untuk berita
Route::get('/berita', function () {
    return view('guests.menus.berita');
});

//routing untuk detail berita
// nama_berita ini diambil berdasarkan link nama berita. bisa juga jadi id nya
Route::get('/berita/nama_berita', function () {
    return view('guests/detail_berita');
});


// routing untuk halaman user

Route::get('/lapor_bencana', function () {
    return view('guests/formlapor');
});
Route::get('/home', 'HomeController@index')->name('home');
//Route::resource('/reports', 'ReportController'); 

/**
 * E-Lapor
 */
Route::get('/elapor-bencana/create', 'ReportController@createReportByGuest')->name('reports.guest.create');
Route::post('/elapor-bencana', 'ReportController@store')->name('reports.store');
Route::get('/elapor-bencana/{report}', 'ReportController@showReport')->name('reports.guest.show');

/**
 * Route Berita
 */
Route::get('/berita', 'AnnouncementsController@getNews')->name('guests.news');

Route::get('/berita/{announcement}', 'AnnouncementsController@showNews')->name('guest_news.show');

Route::get('/kegiatan', 'AnnouncementsController@getActivities')->name('guests.activities');

Route::get('/kegiatan/{announcement}', 'AnnouncementsController@showActivity')->name('guest_activity.show');

Route::get('/findVillages', 'VillageController@findVillagesById')->name('getVillages');

Route::middleware(['auth', 'admin'])->group(function () {
    /**
     * Admin pages' route
     */
    Route::get('/admin', 'HomeController@admin')->name('admin');

    /**
     * Announcement's categories routes
     */
    Route::resource('/categories', 'CategoriesController');

    /**
     * News or Activity route 
     */

    Route::resource('/announcements', 'AnnouncementsController');
    /**
     * Logistik
     */
    Route::resource('/logistics', 'LogisticsController');

    // Cari Logistiks
    Route::get('/search', 'LogisticsController@search')->name('logistics.search');

    /**
     * Jenis Barang logisti
     */
    Route::resource('/logistics-categories', 'LogisticsCategoryController');


    /**
     * Disaster Report route
     */

    Route::get('/laporbencana', 'ReportController@index')->name('reports.index');
    Route::get('/laporbencana/create', 'ReportController@create')->name('reports.create');
    Route::get('/laporbencana/{report}/edit', 'ReportController@edit')->name('reports.edit');
    Route::patch('/laporbencana/{report}', 'ReportController@update')->name('reports.update');
    Route::get('/lapor-bencana/{report}', 'ReportController@show')->name('reports.show');
    Route::delete('/laporbencana/{report}/delete', 'ReportController@destroy')->name('reports.destroy');

    Route::get('/laporbencana/{report}', 'ReportController@validateReport')->name('reports.validate');


    /**
     * Disaster's Route 
     */
    //  Route::resource('/disasters', 'DisasterController');
    Route::get('/jenisbencana', 'DisasterController@index')->name('disasters.index');
    Route::get('/jenisbencana/create', 'DisasterController@create')->name('disasters.create');
    Route::post('/jenisbencana', 'DisasterController@store')->name('disasters.store');
    Route::get('/jenisbencana/{disaster}/edit', 'DisasterController@edit')->name('disasters.edit');
    Route::patch('/jenisbencana/{disaster}', 'DisasterController@update')->name('disasters.update');
    Route::delete('/jenisbencana/{disaster}/delete', 'DisasterController@destroy')->name('disasters.destroy');

    /** Subdistrict's(Kecamatan) route  */
    Route::get('/kecamatan', 'SubdistrictController@index')->name('subdistricts.index');
    Route::get('/kecamatan/create', 'SubdistrictController@create')->name('subdistricts.create');
    Route::post('/kecamatan', 'SubdistrictController@store')->name('subdistricts.store');
    Route::get('/kecamatan/{subdistrict}/edit', 'SubdistrictController@edit')->name('subdistricts.edit');
    Route::patch('/kecamatan/{subdistrict}', 'SubdistrictController@update')->name('subdistricts.update');
    Route::delete('kecamatan/{subdistrict}/delete', 'SubdistrictController@destroy')->name('subdistricts.destroy');

    /** Village's(Desa/Kelurahan) route */
    Route::get('/kecamatan/{subdistrict}/desa', 'VillageController@index')->name('villages.index');
    Route::get('/kecamatan/{subdistrict}/desa/create', 'VillageController@create')->name('villages.create');
    Route::post('/kecamatan/{subdistrict}/desa', 'VillageController@store')->name('villages.store');
    Route::get('/kecamatan/{subdistrict}/desa/{village}/edit', 'VillageController@edit')->name('villages.edit');
    Route::patch('/kecamatan/{subdistrict}/desa/{village}', 'VillageController@update')->name('villages.update');
    Route::delete('/kecamatan/{subdistrict}/desa/{village}/delete', 'VillageController@destroy')->name('villages.destroy');
});
